# HiCData

1. Config the paramters in config.ini
2. Install the packages (todo: add requirement.txt)
2. Run `python3 powerlaw.py`
4. Fig is 'figs/M_res3.png', points are in 'data/anomalies.txt'


## Parameters
* root_dir: the data dir
* key: the key from chr1 to chr19
* limit: limit of considered range
* threshold: anomaly threshold
* vmax: vmax for drawing
* resolution 
* figsize
* start: the start row for analysis, must be higher than 100
* end: the end row for analysis, must be less than mat.shape[0] -300


