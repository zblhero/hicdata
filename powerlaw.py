import numpy as np

import pylab as plt
#import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp

from utils import *
from scipy import signal


import configparser

args = configparser.ConfigParser()
args.read('config.ini')

key = args.get('general', 'key')
limit = args.getint('general', 'limit')
threshold = args.getint('general', 'threshold')
vmax = args.getint('general', 'vmax')
resolution = args.getint('general', 'resolution')
figsize = args.getint('general', 'figsize')
start = args.getint('general', 'start')
end = args.getint('general', 'end')



def powerlaw(x, a, b):
    #return np.exp(-x**2/(2*sigma**2))
    
    return a*x**(-b)

def fit(M, start, end, size):
    length = end-start
    #print(start, end, length)
    M_pred1 = np.zeros((end-start, end-start))
    M_pred2 = np.zeros((end-start, end-start))
    
    
    for i in range(end-start):
        x_data_1 = np.arange(1, size)
        x_data_2 = np.arange(1, size)

        y_data_1 = M[start+i, start+i+1:start+i+size]
        y_data_2 = M[start+i-size:start+i-1, start+i][::-1]
        #print(i, 'fit', x_data_1.shape, y_data_1.shape, x_data_2.shape, y_data_2.shape)
        #print(i, x_data_1[:20], y_data_1[:20], x_data_2[:20], y_data_2[:20])
        
        try:
            popt1, pcov1 = curve_fit(powerlaw, x_data_1, y_data_1, p0=(10, 3.0))

            y_pred1 = powerlaw(x_data_1, *popt1)
        except RuntimeError:
            y_pred1 = y_data_1[:]
            print('runtime error 1', len(y_data_1[:]))
        except TypeError:
            y_pred1 = y_data_1[:]

        try:
            popt2, pcov2 = curve_fit(powerlaw, x_data_2, y_data_2, p0=(10, 3.0))

            y_pred2 = powerlaw(x_data_2, *popt2)
        except RuntimeError:
            y_pred2 = y_data_2[:]
        except TypeError:
            y_pred2 = y_data_2[:] 
        

        if i > 0 :
            M_pred2[max(0, i-size+1):i, i] = y_pred2[:min(i, size-1)][::-1]
        M_pred1[i, i+1:min(length, size+i)] = y_pred1[:min(length-i-1, size-1)]

        
    
    #draw_mat(M_pred1, save='figs/M_pred1.png')
    #draw_mat(M_pred2, save='figs/M_pred2.png')
    
    
    
    M_pred = (M_pred1+M_pred2)/2
    M_origin = M[start:end, start:end]
    
    draw_mat(M_pred, save='figs/M_pred.png', figsize=figsize)
    
    for i in range(end-start):
        for j in range(end-start):
            if i >= j:
                M_origin[i][j] = 0
            if j > i+limit:
                M_origin[i][j] = 0
    
    np.savetxt('data/origin.txt', M_origin)
    np.savetxt('data/pred.txt', M_pred)
    return M_origin, M_pred
    
def post_process(M_origin, M_pred, start, end):
    
    residual = M_origin - M_pred
    
    #print(residual.shape, residual.max(), residual.min(), residual[0])
    
    #residual = residual[residual > 1]
    residual = np.where(residual>1, residual, 0)
    #print('residual', residual.shape, residual.max(), residual.min(), residual[0, :50], M_pred[0])
    draw_mat(residual, save='figs/M_res0.png')
    
    for i in range(end-start):
        for j in range(i+1, end-start):
            # drop isolate points
            #if i>1 and j>1 and np.sum(residual[i-1:i+2, j-1:j+2]) - residual[i][j] == 0:
            #    residual[i][j] = 0
            
            if M_pred[i][j] == 0:
                residual[i][j] = 0
            elif M_pred[i][j] < 0.4:
                residual[i][j] = residual[i][j]/0.4
            else:
                residual[i][j] = residual[i][j]/M_pred[i][j]
                
    #print('residual 1', residual.shape, residual.max(), residual.min(), residual[0, :50])
    draw_mat(residual, save='figs/M_res1.png', vmax=16)
    
    
    
    
    #scharr = np.array([[ -1, -1, -1],[-1,   9, -1],[ -1, -1, -1]]) # Gx + j*Gy
    #scharr = np.array([[1, 2, 1],[2, 4, 2],[1, 2, 1]])
    #scharr = np.array([[1, 2, 1],[0, 0, 0],[-1, -2, -1]])
    #scharr = np.array([[0, -1, 0],[-1, 4, -1],[0, -1, 0]])
    #scharr = np.array([[-1, -1, -1],[0, 0, 0],[1, 1, 1]])
    #grad = 1/16*signal.convolve2d(residual, scharr, boundary='symm', mode='same')
    #grad = 1/16*signal.convolve2d(grad, scharr, boundary='symm', mode='same')
    #grad = 1/16*signal.convolve2d(grad, scharr, boundary='symm', mode='same')
    #grad = residual

    #print(grad.shape, grad[0])
    #np.savetxt('data/grad.txt', grad)
    #draw_mat(grad, save='figs/M_res2.png')
    return residual
    
def find_anomaly(M, mat, positions, start, start_pos):
    E = np.zeros(M.shape)
    #E_origin = np.zeros((M.shape[0]+300, M.shape[1]+300))
    
    anomalies = []
    max_value = 0

    radius = 4
    print(M.shape, M.max(), M.min())
    
    #print('start', start*resolution, mat.shape)
    #print('end', (M.shape[0]+start)*resolution, len(positions))
    
    for i in range(M.shape[0]-radius):
        for j in range(i, min(i+limit, M.shape[1]-radius)):
            sum = np.sum(M[i:i+radius, j:j+radius])
            if sum>threshold:
                #E[i:i+radius, j:j+radius] = M[i:i+radius, j:j+radius]
                E[i:i+radius, j:j+radius] = M[i:i+radius, j:j+radius]
                #print(i+start, j+start, mat[i+start][j+start], positions[i+start+start_pos], positions[j+start+start_pos])
                if mat[i+start][j+start] > 0:
                    print(i, j, start, mat[i+start][j+start])
                    #anomalies.append(((i+start)*resolution, (j+start)*resolution, sum))
                    anomalies.append((positions[i+start+start_pos], positions[j+start+start_pos], sum))
            if sum > max_value:
                max_value = sum
    
    #E_origin[300:-300, 300:-300] = E
    draw_mat(E, save='figs/M_res3.png', vmax=5, cmap='Purples', figsize=figsize)
    with open('data/anomalies.txt', 'w') as f:
        f.write('\n'.join([str(a) for a in anomalies]))
    print(max_value, anomalies, E.max())

def main():
    
    load = True

    if load:
        bases = read_pos()
        mat, start_pos, positions = read_mat(bases, key)


        print(start, end, vmax, figsize, mat[10115][10187])
        draw_mat(mat[start:end, start:end], save='figs/M_origin.png', vmax=vmax, figsize=figsize)
        M1, M2 = fit(mat, start, end, 300)
    
    else:
        M1 = np.loadtxt('data/origin.txt')
        M2 = np.loadtxt('data/pred.txt')

        #start = 5000
        #end = 5600


    
    

    grad = post_process(M1, M2, start, end)
    
    print('start pos', start, start_pos)
    find_anomaly(grad, mat, positions, start, start_pos)
    
    



if __name__ == "__main__":
    main()