import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False

import sys
if False:
    reload(sys)
    sys.setdefaultencoding('utf-8')
else:
    plt.switch_backend('agg')
    
import configparser
args = configparser.ConfigParser()
args.read('config.ini')

root_dir = args.get('general', 'root_dir')

    
#from model import *
#from rpca import R_pca

#root_dir = '/dataset/user/858f2ba0-e230-11e8-b0aa-fa163ee59f29/1802/'

def read_pos():
    bases = {}
    #with open(root_dir+'P3_5000_abs.bed') as f:
    with open(root_dir+'P3_10000_raw_abs.bed') as f:
        chr_lines = f.readlines()
        for line in chr_lines:
            values = line.strip().split('\t')
    
            # chr1	54870000	54880000	5488   
            #       start       end         bin_num
            
            if values[0] in bases:    
                if values[1] in bases[values[0]]:
                    bases[values[0]][int(values[3])].append(values[1:3])
                else:
                    bases[values[0]][int(values[3])] = values[1:3]
            else:
                
                bases[values[0]] = {int(values[3]): values[1:3]}
                print('bed values', values[0], bases[values[0]])
                
    for key in bases.keys():
        base_keys = list(bases[key].keys())
        print(key, len(base_keys), np.max(base_keys), np.min(base_keys))
        
    return bases

def read_mat(bases, key):
    pos = bases[key]
    positions = list(pos.keys())
    start = np.min(positions)
    mat = np.zeros((len(pos), len(pos)))
    
    num = 0
    #with open(root_dir+'P3_5000.matrix') as f:
    with open(root_dir+'P3_10000_raw.matrix') as f:
        lines = f.readlines()
        for line in lines:
            values = line.strip().split('\t')  # '38061', '40244', '1'
            
            
            if int(values[0]) in pos and int(values[1]) in pos:
                num+=1
                if num <10:
                    print(values, pos[int(values[0])], pos[int(values[1])])
                mat[int(values[0])-start][int(values[1])-start] = float(values[2])
                mat[int(values[1])-start][int(values[0])-start] = float(values[2])
    
    print(np.sum(mat), np.max(mat), np.min(mat), key, start)
    return mat, start, pos


        
def normalize(mat):
    for i in range(mat.shape[0]):
        if np.max(mat[i, :])-np.min(mat[i, :]) > 0:
            mat[i, :] = (mat[i, :]-np.min(mat[i, :]))/(np.max(mat[i, :])-np.min(mat[i, :]))
    return mat


def convert_mat(mat, start, end):
    row = end-start+1
    M = np.zeros((row, 2*row-1))
    for i in range(row):
        for j in range(2*row-1):
            if j < row:
                M[i][j] = mat[i+start][end-j+i-1]
            else:
                M[i][j] = mat[i+start][j-row+i+start]
                
    return M


def fixed_mat(mat, start, end, length):
    row = end-start+1
    M = np.zeros((row, length))
    for i in range(row):
        M[i, :] = mat[i+start, i+start:i+start+length]
    return M

def normalize(M):
    for i in range(M.shape[0]):
        M[i, :] = M[i, :]/(M[i, :].max())
    return M

def draw_mat(mat, start=0, end=-1, vmin=0, vmax=4, figsize=30, save=None, show=True, cmap='Reds'):
    mat = mat[start:end, start:end]
    fig, ax = plt.subplots(1, 1, figsize=(30, 30))
    plt.imshow(mat, cmap=cmap, vmin=vmin, vmax=vmax)
    if show:
        plt.show()
    if save is not None:
        plt.savefig(save)