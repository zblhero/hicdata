import numpy as np

import argparse
import os

def process(input_file, output_file):
    with open(input_file) as f, open(output_file, 'w') as fout:
        for line in f.readlines():
            values = line.strip().split()
            print('read line', values)
            
            command = '''java -Xms512m -Xmx2048m -jar juicer_tools_linux_0.8.jar dump observed none l50_td_p_hg19.bwt2pairs.validPairs.hic %s:%s:%s %s:%s:%s BP 5000 > tmp.txt'''%(values[0][3:], values[1], values[2], values[3][3:], values[4], values[5])
            print('excuting command:', command)
            ret = os.system(command)

            if ret != 0:
                exit('system error!')

            result = 0.0
            with open('tmp.txt') as tmp:
                for line1 in tmp.readlines():
                    if not line1.startswith('HiC'):
                        result += float(line1.split()[2])
            line = line.strip()+'\t'+str(result)+'\n'
            print('generating new line:', line)

            fout.write(line)




if __name__ == "__main__":
    process('input.txt', 'output.txt')